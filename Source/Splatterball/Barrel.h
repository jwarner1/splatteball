// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Barrel.generated.h"

UCLASS()
class SPLATTERBALL_API ABarrel : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ABarrel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	static const int m_textureSize = 512;
	/** Sphere mesh that is our acting barrell */
	UPROPERTY()
	UStaticMeshComponent* Barrel;
	UPROPERTY()
	UTexture2D * m_dynamicTexture;
	UPROPERTY()
	UMaterialInterface * m_dynamicMaterial;
	UPROPERTY()
	UMaterialInstanceDynamic * m_dynamicMaterialInstance;

	uint8 m_pixelArray[m_textureSize * m_textureSize]; // Array the size of the texture
	FUpdateTextureRegion2D m_wholeTextureRegion;
};
