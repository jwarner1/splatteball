// Fill out your copyright notice in the Description page of Project Settings.

#include "Barrel.h"
#include "Engine/CollisionProfile.h"


// Sets default values
ABarrel::ABarrel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Barrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Barrel"));
	RootComponent = Barrel;

	Barrel->SetCollisionProfileName(UCollisionProfile::BlockAllDynamic_ProfileName);
	{
		//Adds the static mesh for the Barrel actor
		static ConstructorHelpers::FObjectFinder <UStaticMesh> asset(TEXT("StaticMesh'/Game/Geometry/Meshes/Cylinder.Cylinder'"));
		Barrel->SetStaticMesh(asset.Object);
	}

	

	{
		//gets the material that will be added to the Barrel Actor
		static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/Materials/BarrelMat.BarrelMat'"));
		m_dynamicMaterial = asset.Object;
	}
	/*
	Not currently utilized for dynamic texture updating, Barrel was designed to be a static object
	if (!m_dynamicTexture)
	{
		m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8);
		m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		m_dynamicTexture->SRGB = 0;
		m_dynamicTexture->UpdateResource();
	}

	// Propagate memory's array to the texture.
	if (m_dynamicTexture)
		m_dynamicTexture->UpdateTextureRegions(0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray);
	*/
}

// Called when the game starts or when spawned
void ABarrel::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABarrel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABarrel::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Create a dynamic material instance to swap in the BarrelText texture.
	if (m_dynamicMaterial)
	{
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
		m_dynamicMaterialInstance->SetTextureParameterValue("BarrelText", m_dynamicTexture);
	}

	// Set the dynamic material to the mesh (only if memory changed)
	if (m_dynamicMaterialInstance)
		Barrel->SetMaterial(0, m_dynamicMaterialInstance);
}
