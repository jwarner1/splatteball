// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SplatterballGameMode.h"
#include "SplatterballHUD.h"
#include "SplatterballCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASplatterballGameMode::ASplatterballGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASplatterballHUD::StaticClass();
}
