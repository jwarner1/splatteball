// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Splatterball : ModuleRules
{
	public Splatterball(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core","CoreUObject", "Engine",
            "InputCore", "HeadMountedDisplay", "ProceduralMeshComponent", "RuntimeMeshComponent"});
    }
}
