// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SplatterballProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"

ASplatterballProjectile::ASplatterballProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ASplatterballProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	{
		//refers to the decal to spawn upon creation of projectile
		static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/Materials/PB_Splat.PB_Splat'"));
		ActionDecaltoSpawn = asset.Object;
	}
}

void ASplatterballProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//sets up to spawn a decal with a proper rotation based on the impact of the projectile
	FRotator RandomDecalRotation = Hit.ImpactNormal.Rotation();
	RandomDecalRotation.Roll = FMath::FRandRange(-180.0f, 180.0f);
	//spawns decal with previous setup data
	UGameplayStatics::SpawnDecalAttached(ActionDecaltoSpawn, FVector(32.0f, 64.0f, 64.0f),
		Hit.Component.Get(), Hit.BoneName,
		Hit.ImpactPoint, RandomDecalRotation, EAttachLocation::KeepWorldPosition,
		2.0f);

	Destroy();
}